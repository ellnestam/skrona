So you wanted to see all the bells and whistles. We know, seeing is believing. We're glad you're here.

Here's what you can do:

1. Put a nice picture at the top
2. A header (always there)
3. Text
4. ... and finally, a little soundplayer, ready to play an awesone sound at the bottom.
(5. Oh yeah, the choices. But you knew about them already.)

What do you think? Neat, huh!

Well, that's it.  Now it's time for you to get started. Don't you think?

1. Yeah, download an example!
2. I need to digest this. Go over it one more time. From the top!


